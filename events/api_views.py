from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
import json



class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


'''old code
# class ConferenceListEncoder(ModelEncoder):
#    model = Conference
#    properties = ["name"]


# class ConferenceDetailEncoder(ModelEncoder):
#    model = Conference
#    properties = ["name", "starts",
#                   "ends", "description",
#                  "created","updated",
#                  "max_presentations", "max_attendees",
#                 ]
'''
class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]

@require_http_methods(["GET", "POST", "PUT", "DELETE"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content['location'])
            content['location'] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid location id'},
                status=400,
            )
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder = ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "conferences" in content:
                conferences = Conference.objects.get(location["conferences"])
                content["conferences"] = conferences
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
    else: #DELETE
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})




    """ what this does
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """

    '''old code
    # response = []
    # conferences = Conference.objects.all()
    # for conference in conferences:
    #     response.append(
    #         {
    #             "name": conference.name,
    #             "href": conference.get_api_url(),
    #         }
    #     )
    # return JsonResponse({"conferences": response})
'''

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

@require_http_methods([ "GET", "POST", "PUT", "DELETE"])
def api_show_conference(request, id):
    if request.method == "GET":

        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(content)
        try:
            conference = Conference.objects.get(id=id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "conference does not exist"},
                status=400
            )
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(conference=content["conference"])
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "conference does not exist"},
                status=400,
            )
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False
        )
    else:
        count, _ = Conference.Objects.get(id=id).delete()
        return JsonResponse({"deleted"})


@require_http_methods(["GET", "POST", 'PUT', 'DELETE'])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    elif request.method == 'POST':
        content = json.loads(request.body)
        try:
           # Get the State object and put it in the content dict
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "conferences" in content:
                conferences = Conference.objects.get(locations=content["conferences"])
                content["conferences"] = conferences
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": " Invalid Location/conference "}
            )
        Location.objects.filter(id=id)
    else: #DELETE
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({'deleted': count > 0})
        """

    Lists the location names and the link to the location.

    Returns a DICTIONARY with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.
    """
''' old code^
# response = []
    # locations = Location.objects.all()
    # for location in locations:
    #     response.append(
    #         {
    #             "name": location.name,
    #             "href": location.get_api_url(),
    #         }
    #     )
    # return JsonResponse({"locations": response})
'''

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
    ]
    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}

@require_http_methods(["GET", "DELETE", "PUT", "POST"])
def api_show_location(request, id):
    if request.method =="GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    elif request.method == "PUT":  #PUT
        #copied from create
        content = json.loads(request.body)
        try:
            #new code to get foreignkey
            if 'state' in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid state abbreviation'},
                status=400,
            )
        #new
        Location.objects.filter(id=id).update(**content)
        #from get detail
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    else:  #POST
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=id)
            content['location'] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid Location id' },
                status=400,
            )
        Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )



'''
    #  old version of this^  #
    #         "name": location.name,
    #         "city": location.city,
    #         "room_count": location.room_count,
    #         "created": location.created,
    #         "updated": location.updated,
    #         "state": {
    #             "name": location.state.name,
    #             "href": location.get_api_url(),
    #         },
    #     }
'''
""" ^ Returns the DETAILS for the Location model specified
#     by the id parameter.

#     This should return a DICTIONARY with the name, city,
#     room count, created, updated, and state abbreviation."""
"""
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.
"""
