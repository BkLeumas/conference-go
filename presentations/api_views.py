from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation
from django.views.decorators.http import require_http_methods
import json
# from events.models import Status, Conference

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["name"]
@require_http_methods(["GET", "POST", "PUT", "DELETE"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(id=conference_id)
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            presentation = Presentation.objects.get(id=conference_id)
            content["presentation"] = presentation
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Presentation does not exist"},
                status=400,
            )
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationListEncoder,
            safe=SUBMITTED,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "status" in content:
                status = Status.objects.get(content=["presentations"])
                content["status"] = status
        except Status.DoesNotExist:
            return JsonResponse(
                {'message': 'status does not exist'}
            )
        try:
            if "conference" in content:
                conference = Event.Conference.object.get(content=["presentations"])
                content["presentations"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "conference does not exist"}
            )
        Presentation.objects.filter(id=id)
    else:#DELETE
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


''' old code >
   # presentations = [
    #     {
    #         "title": p.title,
    #         "status": p.status.name,
    #         "href": p.get_api_url(),
    #     }
    #     for p in Presentation.objects.filter(conference=conference_id)
    # ]
    # return JsonResponse({"presentations": presentations})
'''
class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]
@require_http_methods(["GET", "POST", "PUT", "DELETE"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            presentation = Presentation.objects.get(id=id)
            content["presentation"] = presentation
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Presentation does not exist"},
                status=400,
            )
        Presentation.objects.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    # elif request.method == "PUT":
    #     content = json.loads(request.body)
    #     try:
    #         if "status" in content:
    #             status = Status.objects.get(presentations=content["status"])
    #             content["status"] = status
    #     except Status.DoesNotExist:
    #         return JsonResponse(
    #             {'message': 'presentation did not have a status'},
    #         )
    #     try:
    #         if "events.conference" in content:
    #             conference = Presentation.objects.get
    #     except:

    else: #DELETE
        count, _ = Presentation.object.get(id=id).delete()
        return JsonResponse({"deleted"})



'''
    #     {
    #         "presenter_name": Presentation.presenter_name,
    #         "company_name": Presentation.company_name,
    #         "presenter_email": Presentation.presenter_email,
    #         "title": Presentation.title,
    #         "synopsis": Presentation.synopsis,
    #         "created": Presentation.created,
    #         "status": Presentation.status,
    #         "conference": {
    #             "name": Presentation.conference.name,
    #             "href": Presentation.conference.get_api_url(),
    #         },
    #     }
    # )
  '''


'''
    #     "presenter_name": the name of the presenter,
    #     "company_name": the name of the presenter's company,
    #     "presenter_email": the email address of the presenter,
    #     "title": the title of the presentation,
    #     "synopsis": the synopsis for the presentation,
    #     "created": the date/time when the record was created,
    #     "status": the name of the status for the presentation,
    #     "conference": {
    #         "name": the name of the conference,
    #         "href": the URL to the conference,
    #     }
    # }
'''
